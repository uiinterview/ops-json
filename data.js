export default {
    "merchantId": "3roses",
    "campaignId": 107201,
    "campaignName": "test-camp",
    "campaignCode": "test_camp_489_2019_oct_24",
    "format": "MediumRectangle",
    "creativeLine1": "",
    "creativeLine2": "",
    "creativeImage": "",
    "callToAction": "Buy Now",
    "shortListType": "CUSTOM",
    "productIds": [
        "3ROS422FD2F99FDF44A89BABF46A59512351"
    ],
    "shortList": [
        "3ROS422FD2F99FDF44A89BABF46A59512351"
    ],
    "shortListRule": null,
    "budgetRule": {
        "LIFE_TIME": 300.0
    },
    "properties": {
        "fullStoreMode": "false",
        "bannerMedia": "",
        "merchantRule": "INCLUDE",
        "isEditedFromNonStudio": "true",
        "studioTemplateId": "story_board",
        "multiMerchantMode": "true",
        "canvas": "true",
        "campaignReportMailingList": "priya.singh@shopalyst.com",
        "merchants": "shoppersstop,mtrfoods,humarashop",
        "shortlystMode": "PRDGRP"
    },
    "bannerProperties": null,
    "widgetProperties": null,
    "campaignCheckoutMode": null,
    "mediaProperties": null,
    "status": "active",
    "startTime": "2019-10-24 00:00",
    "endTime": "2020-01-24 00:00",
    "binCampaign": false,
    "binProperties": null,
    "variantProperties": null,
    "landingProperties": {
        "widgets": [
            {
                "id": "Banner",
                "props": {
                    "slides": [
                        {
                            "media": {
                                "type": "yt",
                                "videoId": "AMq2uNdMkJQ"
                            },
                            "tags": [
                                "rghtj"
                            ]
                        }
                    ],
                    "startSlide": 0,
                    "title": ""
                }
            },
            {
                "id": "Video",
                "props": {
                    "slides": [
                        {
                            "media": {
                                "type": "yt",
                                "videoId": "AMq2uNdMkJQ"
                            },
                            "tags": [
                                "wefgdfsf"
                            ]
                        }
                    ],
                    "startSlide": 0,
                    "title": ""
                }
            },
            {
                "id": "Image",
                "props": {
                    "slides": [
                        {
                            "media": {
                                "url": "https://images.pexels.com/photos/2597358/pexels-photo-2597358.jpeg",
                                "type": "image"
                            },
                            "audienceSegment": [
                                "test"
                            ]
                        }
                    ],
                    "startSlide": 0,
                    "title": ""
                }
            },
            {
                "id": "ProductCarousel",
                "props": {
                    "eans": [
                        "8901030194160"
                    ],
                    "title": ""
                }
            },
            {
                "id": "Conversational",
                "props": {
                    "steps": [
                        {
                            "conversationStarter": "Hi Select some",
                            "questions": [
                                {
                                    "question": "product and articles",
                                    "answers": [
                                        {
                                            "type": "product",
                                            "ean": "8901030194160"
                                        },
                                        {
                                            "type": "article",
                                            "inspiration": "4A1323F2D9DC0B89256E42A718977AB1"
                                        },
                                        {
                                            "type": "article",
                                            "inspiration": "97C813EF88D43484B17146718723B054"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "title": ""
                }
            }
        ],
        "theme": {
            "palette": {
                "primary": {
                    "main": "#7B708F"
                },
                "secondary": {
                    "main": "#AF251C"
                }
            },
            "background": "url(https://images.pexels.com/photos/1694980/pexels-photo-1694980.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500)"
        }
    },
    "merchantProperties": null,
    "updatedBy": "jithin@shopalyst.com",
    "updatedDate": "Wed Jan 22 12:12:39 UTC 2020",
    "active": false
}