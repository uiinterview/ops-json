**Get data from json and write a function to modify few values and print the updated JSON**

## Edit a file

No modifications should be done on files directly.

## Write a function in index.js

Expectations:

1. This function should access data from JSON file.
2. change the status to inactive.
3. Under landingProperties > widgets Array do the following:
    a. for id = Image modify the media Object (type/url).
    b. for id = Conversational change the question in questions array.
4. Console the modified JSON

Testing:
1. Install http-server (https://www.npmjs.com/package/http-server)

Total time for this activity is 1 hour.

